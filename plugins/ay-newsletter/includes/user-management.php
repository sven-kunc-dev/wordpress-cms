<?php

namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

class NlUserManagement {

	public function __construct() {
		// to show a new entry in user admin page for bulk register users to newsletter
		add_filter( 'bulk_actions-users', array( $this, 'nl_register_into_user_menu' ) );
		add_filter( 'handle_bulk_actions-users', array( $this, 'nl_handle_bulk_registration_user' ), 10, 3 );
		add_action( 'admin_notices', array( $this, 'nl_output_message_registered' ) );

		// to show a new column in user management page
		add_filter( 'manage_users_columns', array( $this, 'nl_modify_user_table' ) );
		add_filter( 'manage_users_custom_column', array( $this, 'nl_insert_data_to_user_table' ), 10, 3 );
	}

	/**
	 * This function registers two menus in bulk actions menu of user page
	 */
	function nl_register_into_user_menu( $bulk_actions ) {
		$bulk_actions['nl_register_user'] = __( 'Register to Newsletter', DOMAIN );
		$bulk_actions['nl_remove_user']   = __( 'Remove from Newsletter', DOMAIN );

		return $bulk_actions;
	}

	/**
	 * This function implements the logic of the bulk action
	 */
	function nl_handle_bulk_registration_user( $redirect, $action, $user_ids ) {
		/* $redirect has to be null, because it will send the current URL and we don't need the GET parameters
		 * (e.g. current URL is: [WORDPRESS]/wp-admin/users.php?nl_bulk_register_users=4
		 *  without $redirect = null the new URL would be: [WORDPRESS]/wp-admin/users.php?nl_bulk_register_users=4&nl_bulk_remove_users=4)
		 * This would be wrong!
		 */
		$redirect = null;
		if ( $action === 'nl_register_user' ) {
			foreach ( $user_ids as $user_id ) {
				update_user_meta( $user_id, 'nl_registered', true );
				$state = 'nl_bulk_register_users';
			}
		} elseif ( $action === 'nl_remove_user' ) {
			foreach ( $user_ids as $user_id ) {
				update_user_meta( $user_id, 'nl_registered', false );
				$state = 'nl_bulk_remove_users';
			}
		}

		$redirect = add_query_arg( $state, count( $user_ids ), $redirect );

		return $redirect;
	}

	/**
	 * This function outputs a feedback for the administrator
	 */
	function nl_output_message_registered() {

		// The echo <div>(...)</div> has to be in the function, otherwise it will be echoed on every page. This would be barely visible in the backend.
		if ( isset ( $_REQUEST['nl_bulk_register_users'] ) ) {
			echo '<div id="message" class="updated fade">';
			// sanitizing the value (XSS protection)
			$count = intval( $_REQUEST['nl_bulk_register_users'] );
			echo $count === 1 ? $count . ' ' . __( 'user registered to newsletter.',
					DOMAIN ) : $count . ' ' . __( 'users registered to newsletter.', DOMAIN );
			echo '</div>';
		} elseif ( isset( $_REQUEST['nl_bulk_remove_users'] ) ) {
			echo '<div id="message" class="updated fade">';
			$count = intval( $_REQUEST['nl_bulk_remove_users'] );
			echo $count === 1 ? $count . ' ' . __( 'user removed from newsletter.',
					DOMAIN ) : $count . ' ' . __( 'users removed from newsletter.', DOMAIN );
			echo '</div>';
		}
	}

	/**
	 * This function adds a column to user management page
	 */
	function nl_modify_user_table( $column ) {
		$column['nl'] = __( 'Newsletter User', DOMAIN );

		return $column;
	}

	/**
	 * This function fill a chosen column with data
	 */
	function nl_insert_data_to_user_table( $value, $column, $user_id ) {
		if ( $column === 'nl' ) {
			if ( get_user_meta( $user_id, 'nl_registered', true ) == true ) {
				$value = __( 'Yes', DOMAIN );
			} else {
				$value = __( 'No', DOMAIN );
			}
		}

		return $value;
	}


}