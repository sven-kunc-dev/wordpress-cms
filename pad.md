Ideen für Funktionen im Fakultät-IN Projekt

Ideen von dem Zettel:

    Editor für Events mit Termin, Ort, Titel, Bildern und Beschreibung; Anzzeige dann mit Karte

    alternativ integration eine ical Kalenders (Vorteil; wird meist besser gewartet)

    Übersicht aller Professoren; mit Bild eigene Seite für jeden Prof => Filterbar nach Gebaude

    Plan mit Vorlesungen; Aktuelle VL mit Raum auf Startseite => Eventuell mit einer kurzen Kommentarfunktion für den Dozenten, welcher nach einer bestimmten Zeit gelöscht wird (spontane Raumänderung bspw.)

    Bei Gebäuden weiß ich nicht genau worauf er hinaus will. Möglich wäre aber eine Karte mit hover und click. Vgl: http://fdg-ab.de/ueber_uns/partnerschulen/ (hab ich gebaut; schaff ich also auch nochmal; ist sogar schon wordpress) Jedes Gebäude hätte dann seinen eig. Seite

    Themen fur Abschlussarbeiten: Eventuell eine Auflistung "aller" eingetragenen Abschlussarbeiten, die Professoren bieten. Eventuell auch Eintragung einer Idee von Studenten mit Auswahl eines Professors, der sich zu dem Moment als "offen/frei" markiert, der dann eine E-Mail bei Anfragen bekommt. Natürlich nur bei Professoren, die die Idee anfangs digital haben möchten. (Wäre auch fûr mehr Themen verwendbar, wie IT-Projekt bspw.)

    3D-Würfel auf der Startseite zur Navigation zwischen den einzelnen Features 

    Social Media Geschichten: Vernetzen, Kontake, Messenger, Likefunktion



Weitere Ideen:

    Login bereich? => Ja, per LDAP bspw. an das Hochschulsystem? Womöglich geben die uns keinen Zugriff. Aber rein von der Idee her (machen es dann eh mit Testsätzen)

    Speiseplan der Mensa.

    Echter support von Mehrsprachigkeit

    Linksammlung  - wohl keine Meisterleistung

    Blogging-/Newsmöglichkeit, sozusagen Standard-Wordpress, für Benachrichtigungen bei Updates bspw.

    Content Filterung

    Search Engine Optimization


Sollen wir beispieldaten einfügen? In welchem Umfang?
=> Zu jedem Feature ein paar Testsätze

Zusammenfassung Vortrag vom 04.04.  (gewünschte Features):

    Online Visitenkarte des TH-Mitglieds (Prof, MA, Fachschaften, Student,..)

    Termine und Veranstaltungen mithilfe eines Kalenders darstellen, bearbeiten, teilnehmen, folgen

    digitale Navigation (Türschilder) anhand eines grafischen Lageplans

    Abschlussarbeiten (Wer macht wann was und wo?)

    Forschungsbereiche der Professoren/Hochschule visualisieren

    Besucherfunktion (nur Frontend), Kommentarfunktion für Studenten (Leserechte), Editierrechte für Professoren und Mitarbeiter (Lese- und Schreibrechte), Alles (Admin) 
