<?php get_header(); ?>

<?php if ( have_posts() ): ?>
    <h1 class="article-title"> Suchergebnisse f&uuml;r <span style="color: #0046A0;"><?php echo $s ?></span></h1>
	<?php get_template_part( 'template-parts/content/content' ); ?>

    <p><?php next_posts_link( '&laquo; &Auml;ltere Eintr&auml;ge' ); ?>
		<?php previous_posts_link( 'Neuere Eintr&auml;ge &raquo;' ); ?></p>

<?php else: ?>
    <h1 class="article-title"> Suchergebnisse</h1>
    <h2 class="entry">Die Suche f&uuml;r <span style="color: #0046A0;"><?php echo $s ?></span> war erfolglos.</h2>
<?php endif; ?>

<?php get_footer(); ?>