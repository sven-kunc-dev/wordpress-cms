<?php

defined( 'ABSPATH' ) or die( "Cannot access directly" );

global $wpdb;

$sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . "pm_documents";
$wpdb->query( $sql );

$sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . "pm_projects";
$wpdb->query( $sql );