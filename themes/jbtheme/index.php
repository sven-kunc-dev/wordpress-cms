<?php
wp_enqueue_style("bootstrap",  get_template_directory_uri() . "/includes/bootstrap.min.css");
wp_enqueue_script("bootstrap",  get_template_directory_uri() . "/includes/bootstrap.min.js");

get_header();
?>
<div class="row">
    <div id="cont" class="col-md-6 offset-md-2">
        <?php get_template_part("loop", "index"); ?>
    </div><!--#cont-->
</div>    
<?php get_footer(); ?>

