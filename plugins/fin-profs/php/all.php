<?php
/*
this is done by a page template and not by an archive page for the reditor can add custom content in top of the list of professors
*/

get_header();

while (have_posts()): the_post(); ?>
    <?php get_template_part( 'template-parts/content/content', 'title' ); ?>
    <div class="row">
    <article class="entry">
        <?php the_content(); ?>
    </article>
<?php endwhile; ?>

<div id='profs'>
<?php $args = array(
    'posts_per_page'   => 5,
    'offset'           => 0,
    'orderby'          => 'title',
    'order'            => 'ASC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'professor',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true,
);
$profs = get_posts( $args ); 
foreach($profs as $prof){
    echo "<div class='prof'>";
    echo "<a href='" . get_the_permalink($prof) . "'>";
    echo "<h3>" . get_the_title($prof) . "</h3>";
    //echo "<hr />";
    echo "<img src='" . get_field("photo",$prof->ID) . "' />";
    ?>
    <div class="details">
        <?php echo __("Subject", "fin-profs") . ": " . get_field("subject", $prof->ID); ?> <br />
        <?php echo __("Email address", "fin-profs") . ": " . get_field("email", $prof->ID); ?> 
    </div>
    <?php
    echo "</a></div>";
}
get_footer();
?>
         
