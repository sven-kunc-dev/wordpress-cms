<?php if ( is_active_sidebar( 'right_sidebar' ) ) : ?>
    <aside class="sidebar">
			<?php dynamic_sidebar( 'right_sidebar' ); ?>
        <div class="padder"></div>
    </aside> <!-- .sidebar -->
<?php endif; ?>
