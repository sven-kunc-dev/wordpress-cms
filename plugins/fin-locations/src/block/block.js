/**
 * BLOCK: fin-locations
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import ImageMapEditor from "./ImageMapEditor.js"

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType,

} = wp.blocks; // Import registerBlockType() from wp.blocks
//import MediaUpload from '@wordpress/blocks';
//import Button from '@wordpress/components';


const {
	InspectorControls,
	MediaUpload,
	PlainText

} = wp.editor;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */

registerBlockType( 'cgb/block-fin-locations', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'FIN Location Map', "fin-locations" ), // Block title.
	icon: 'admin-site',
	category: 'common',
	keywords: [
		__( 'Map',"fin-locations" ),
		__( 'Navigation',"fin-locations" ),
	],

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */

	attributes: {
		 backgroundImage: {
			type: 'string',
			default: null, // no image by default!
		},
		imageMapText:{
			type: 'string',
			default: "asdf"
		},
		imageMapHtml:{
			type: 'string',
			default: ""
		}
	},


	edit: function( props ) {
		const {setAttributes, attributes} = props;
		const { backgroundImage} = props.attributes;

		function onImageSelect(imageObject) {
			setAttributes({
				backgroundImage: imageObject.sizes.full.url
			})
		}

		function setImageMapHtml(html){
			console.log(html);
			setAttributes({
				imageMapHtml: html
			})
		}


		return (
			<div>

				<InspectorControls>
					 <div class='my-2'>
						<strong>{__("Select a background image:", "fin-locations")}</strong>
						<MediaUpload
							onSelect={onImageSelect}
							type="image"
							value={backgroundImage}
							render={({ open }) => (
								<Button onClick={open}>
									{__('Upload Image!',"fin-locations")}
								</Button>
							)}
						/>
					</div>
					<ImageMapEditor imageMapHtml={attributes.imageMapHtml} imageMapHtmlChanged={setImageMapHtml}/>
				</InspectorControls>
					<img src={backgroundImage} />
					<br />
					<div>

      </div>

			</div>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		const { backgroundImage, imageMapHtml} = props.attributes;
		return (
			<div>
			<img src={backgroundImage} usemap="#finImgMap" />
			<div  dangerouslySetInnerHTML={{__html: imageMapHtml}}></div>
			</div>
		);
	},
} );
