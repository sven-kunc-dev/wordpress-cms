<?php

namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

class NlAdminMenu {

	// array with parameters
	private $users;

	/**
	 * NlAdminMenu constructor.
	 * Registers Admins menu
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'nl_scripts' ) );
		add_action( 'admin_menu', array( $this, 'nl_add_menu' ) );
		add_action( 'plugins_loaded', array( $this, 'nl_plugins_loaded' ) );

	}

	/**
	 * Adds the admin menu
	 */
	public function nl_add_menu() {
		add_menu_page( __( 'Manage Newsletter', DOMAIN ), __( 'Manage Newsletter', DOMAIN ), 'manage_options',
			'newsletter', array( $this, 'nl_send_mail_menu' ) );
		add_submenu_page( 'newsletter', __( 'Send mail', DOMAIN ), __( 'Send mail', DOMAIN ),
			'manage_options', 'newsletter-send', array( $this, 'nl_send_mail' ) );
		add_submenu_page( 'newsletter', __( 'Show users', DOMAIN ), __( 'Show registered users', DOMAIN ),
			'manage_options', 'newsletter-show', array( $this, 'nl_list_newsletter_user' ) );

		// to remove the first submenu that is added automatically
		remove_submenu_page( 'newsletter', 'newsletter' );
	}

	/**
	 * Defines location of localization files
	 */
	function nl_plugins_loaded() {
		load_plugin_textdomain( DOMAIN, false, '/ay-newsletter/includes/lang' );
	}

	/**
	 * Send mail logic
	 */
	function nl_send_mail() {

		// process to send mail
		if ( isset( $_POST['nl-send-mail'] ) ):
			// sanitize form values
			$message = esc_textarea( $_POST["nl-mail-content"] );
			$subject = sanitize_text_field( $_POST['nl-mail-subject'] );

			$name       = bloginfo( 'name' );
			$admin_mail = get_option( 'admin_email' );
			$headers    = "From: $name <$admin_mail>" . "\r\n";

			// get users. The filter is behind the "args" parameter which was set at line 8.
			// This means: Get all users who has parameter "nl_registered: True" (nl_registered == true)
			$this->users = get_users( ARGS );

			// send mail to all users
			foreach ( $this->users as $u ) {
				wp_mail( $u->user_email, $subject, $message, $headers );
			}
		endif; ?>


        <div class="wrap">
            <h1><?php _e( 'Send email to newsletter users', DOMAIN ); ?></h1>
            <p><?php _e( "Email address", DOMAIN ); ?>
                <strong> <?= get_option( 'admin_email' ); ?> </strong> <?php _e( "will be used.", DOMAIN ); ?></p>
            <form method="post" action="<?= esc_url( $_SERVER['REQUEST_URI'] ); ?>">
                <label for="nl-mail-subject"><?php _e( 'Subject of email', DOMAIN ); ?><br>
                    <input type="text" id="nl-mail-subject" name="nl-mail-subject">
                </label><br>
                <label for="nl-mail-content"><?php _e( 'Content of email', DOMAIN ); ?><br>
                    <textarea rows="10" cols="100" id="nl-mail-content" name="nl-mail-content"></textarea>
                </label><br>
                <input type="submit" name="nl-send-mail" value="Nachricht abschicken">
            </form>
        </div> <!-- #wrap -->
		<?php
	}

	/**
	 * List all users that are registered to newsletter
	 */
	function nl_list_newsletter_user() {
		// process to remove user from newsletter
		if ( isset( $_POST['nl-remove'] ) ):
			update_user_meta( $_POST['nl-user'], 'nl_registered', false ); ?>
		<?php endif; ?>

        <div class="wrap">
            <h1><?php _e( 'Manage registered newsletter user', DOMAIN ); ?></h1>

			<?php $this->users = get_users( ARGS ); ?>

            <table border="1" width="100%">
                <thead>
                <tr>
                    <th><?php _e( 'Name', DOMAIN ); ?></th>
                    <th><?php _e( 'Role(s)', DOMAIN ); ?></th>
                    <th><?php _e( 'Mail address', DOMAIN ); ?></th>
                    <th><?php _e( 'Action', DOMAIN ); ?></th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ( $this->users as $u ): ?>
                    <tr>
                        <td><?= $u->user_nicename; ?></td>
                        <td><?= $u->roles[0]; ?></td>
                        <td><?= $u->user_email; ?></td>
                        <td>
                            <form method="post" action="<?= esc_url( $_SERVER['REQUEST_URI'] ); ?>">
                                <input type="hidden" name="nl-user" id="nl-user" value="<?= $u->ID; ?>"/>
                                <input type="submit" name="nl-remove" value="<?php _e( 'Remove user', DOMAIN ); ?>"/>
                            </form>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
            <?php /* Logic of REST API */ ?>
            <p><?php _e( 'The route/REST API is',
					DOMAIN ); ?> <?= site_url(); ?>/wp-json/newsletter/users</p>
            <p><?php _e( 'This is just an example, how you can gather this list in JSON format via AJAX. If you click this button, please see your network activities or the console in debugging mode of your browser. The textare shows the JSON data stringified. Please look into the code, how you can authenticate your request, if you want to use the REST API.',
					DOMAIN ); ?></p>
            <?php /* In "includes/js/scripts.js" there is a eventhandler assigned to this button. If clicked to this button, then process the content of the eventhandler. */ ?>
            <button id="ajax-tester" name="ajax-tester"><?php _e( 'Export via AJAX', DOMAIN ); ?></button>
            <br><br>
            <textarea id="ajax-result" name="ajax-result" cols="80" rows="20" disabled></textarea>
        </div>

		<?php
	}

	/**
	 * Load necessary javascript
	 */
	function nl_scripts() {
	    // Load JavaScript file and tell them, that jQuery is needed (this will inject necessary jQuery files to the wordpress page)
		wp_enqueue_script( 'newsletter', plugin_dir_url( __FILE__ ) . 'js/scripts.js', [ 'jquery' ], null, true );

		// This is the magic behind the authentication!
        // With this function we can give parameters to the JavaScript file and use is. wp_create_nonce('wp_rest') is necessary, so we can authenticate the request for the REST-API
        // Parameter "path" just gives the path of the current wordpress installation
        // In JavaScript you can access to this parameters with the keyword "Settings"
		wp_localize_script( 'newsletter', 'Settings', array(
			'nonce' => wp_create_nonce( 'wp_rest' ),
			'path'  => get_site_url()
		) );
	}
}