<?php

/*
Plugin Name: Project Management
Description: Manage projects with professors and students. This can be used for graduation work or your semester projects.
Author: Ayberk Yavas
Version: 0.1
Text Domain: projectmanagement
Domain Path: /includes/lang/
*/

namespace plugins\project_management;

//include('includes/admin-page.php'); // admin page Wordpress
include( 'includes/frontend.php' ); // front end Wordpress

function pm_install() {
	include('includes/install/install.php');
}

register_activation_hook( __FILE__, 'plugins\project_management\pm_install' );

function pm_uninstall() {
	include('includes/install/uninstall.php');
}

register_uninstall_hook(__FILE__, 'plugins\project_management\pm_uninstall');

// this can be explained in project documentation (why just uninstall hook and not deactivate hook)
// register_deactivation_hook(__FILE__, 'plugins\project_management\pm_uninstall');

new pmFrontEnd();