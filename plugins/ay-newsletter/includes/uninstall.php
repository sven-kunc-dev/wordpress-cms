<?php

namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( "Cannot access directly" );

// this is needed to find all users with this parameter
$users = get_users('meta_key=nl_registered');

// remove all user meta data ('nl_registered') from database
foreach ($users as $u) {
	delete_user_meta($u->ID, 'nl_registered');
}

?>