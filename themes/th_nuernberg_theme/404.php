<?php get_header(); ?>

<h2 class="article-title"> Fehler 404 - Seite nicht gefunden</h2>
<article class="entry">
    <p>Ihre gew&uuml;nschte Seite konnte leider nicht gefunden werden!</p>
    Eine m&ouml;gliche Ursache ist
    <ol>
        <li>die Seite wurde in der Zwischenzeit verschoben oder gel&ouml;scht</li>
        <li>Sie haben einen Tippfehler in der URL &uuml;bersehen</li>
        <li>Momentan werden Wartungsarbeiten durchgeführt</li>
    </ol>
</article> <!-- .entry -->

<?php get_footer(); ?>

