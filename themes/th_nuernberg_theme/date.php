<?php get_header(); ?>
    <h1 class="article-title"> Alle Artikel vom <?php echo get_the_date( 'd.m.Y' ); ?></h1>
<?php get_template_part( 'template-parts/content/content' ); ?>
<?php get_footer();