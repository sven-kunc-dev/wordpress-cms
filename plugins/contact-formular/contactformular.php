<?php

/*
Plugin Name: Light Contact Form
Description: A light-weighted contact form for Wordpress
Author: Ayberk Yavas
Version: 1.0
Text Domain: contactformular
Domain Path: /includes/lang/
*/

namespace plugins\contact_formular;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

include('includes/admin-page.php'); // admin page Wordpress
include('includes/frontend.php'); // front end Wordpress

if (is_admin()) {
    new CfAdminPage();
}

new CfFrontEnd();

// EOL