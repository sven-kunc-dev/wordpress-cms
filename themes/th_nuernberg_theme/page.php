<?php
get_header();

if ( have_posts() ) {
	the_post();

	get_template_part( 'template-parts/content/content', 'title' );
	get_template_part( 'template-parts/content/content', 'entry' );
}

get_footer();
?>