<?php

/*
Plugin Name: News Letter
Description: Users can register for news letter
Author: Ayberk Yavas
Version: 1.0
Text Domain: newsletter
Domain Path: /includes/lang/
*/

namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

include( 'includes/initialization.php' ); // initialization plugin
include( 'includes/admin-menu.php' ); // for admin menu
include( 'includes/profile-management.php' ); // for profile page
include( 'includes/user-management.php' ); // for user management page
include( 'includes/serialization.php' ); // for serialization

if ( is_admin() ) {
	new NlAdminMenu();
}

/**
 * Start initialization
 */
function nl_register_widget() {
	register_widget( __NAMESPACE__ . '\\NlInitialization' );
}

add_action( 'widgets_init', __NAMESPACE__ . '\\nl_register_widget' );

// uninstall when plugin is deactivated
function nl_uninstall() {
	include( 'includes/uninstall.php' );
}

register_deactivation_hook( __FILE__, __NAMESPACE__ . '\\nl_uninstall' );

new NlProfileManagement();
new NlUserManagement();
new NlInitialization();

// EOL

