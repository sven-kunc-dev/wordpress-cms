

<?php while (have_posts()) {the_post(); ?>
    <h2><?php the_title(); ?></h2>

    <?php if ( $post->post_parent ) { ?>
    Elternseite: <a href="<?php echo get_permalink( $post->post_parent ); ?>" >
        <?php echo get_the_title( $post->post_parent ); ?>
    </a>
    <?php } ?>
    
    <?php
    $children = get_pages( array( 'child_of' => get_the_ID() ) );
    if(count($children) > 0){
        echo "Unterseiten: " . implode(array_map(function($child){
                return " <a href='" . get_permalink($child) . "'>" .  get_the_title($child) . "</a>"; 
        },$children),", ");
    }
    
?>
<?php the_content(); } ?>


