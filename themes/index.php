/*
Theme Name: Fakultät_TH-Nürnberg_Theme
Theme URI: http://example.de
Description: Startseite der
Author: Sven Kunc
Author URI: http://www.example.de
*/
<!-- ------------------------------------INDEX.PHP------------------------------------------- -->
<!DOCTYPE html>

<!-- ------- HEADER (body und main öffnen)------- -->
<?php get_header(); ?>

<!-- Schleife über alle Inhalte, Beiträge, Posts, Content -->
<article id="cont">
<?php while (have_posts()) {
the_post(); ?>
<h2><?php the_title(); ?></h2>
<?php the_content();
} ?>
</article><!-- #cont -->

<!-- ------- SIDEBAR ------- -->
<?php get_sidebar(); ?>

<!-- ------- FOOTER (body und main schließen)------- -->
<?php get_footer(); ?>
