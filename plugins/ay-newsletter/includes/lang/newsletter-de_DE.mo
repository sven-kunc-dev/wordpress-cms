��    !      $  /   ,      �     �     �                    ,  !   >     `  
   e     p     �     �     �     �     �     �     �     �  	          
   /     :     K  M  a     �  "   �     �     �     �          9     X  �  f     �     �               /     >  (   R     {  
   �     �     �     �     �     �     �      	     $	  #   -	  
   Q	     \	     x	     �	     �	  g  �	       %         F  *   I  '   t  +   �  (   �     �               !                   	                                                                                      
                                       Action Content of email Email address Export via AJAX Mail address Manage Newsletter Manage registered newsletter user Name Newsletter Newsletter User Newsletter Widget No Register to Newsletter Registered to Newsletter Remove from Newsletter Remove user Role(s) Send email to newsletter users Send mail Show registered users Show users Subject of email The route/REST API is This is just an example, how you can gather this list in JSON format via AJAX. If you click this button, please see your network activities or the console in debugging mode of your browser. The textare shows the JSON data stringified. Please look into the code, how you can authenticate your request, if you want to use the REST API. Title True, if registered to newsletter. Yes user registered to newsletter. user removed from newsletter. users registered to newsletter. users removed from newsletter. will be used. Project-Id-Version: 
POT-Creation-Date: 2019-07-02 14:08+0200
PO-Revision-Date: 2019-07-02 14:08+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__;__;_
X-Poedit-SearchPath-0: .
 Aktion Inhalt des E-Mails E-Mail-Adresse Exportieren via AJAX E-Mail-Adresse Verwalte Newsletter Verwalte angemeldete Newsletter-Benutzer Name Newsletter Newsletter-Benutzer Newsletter Widget Nein Registriere zum Newsletter Angemeldet zum Newsletter Melde vom Newsletter ab Melde Benutzer vom Newsletter ab Rolle(n) Sende E-Mail an Newsletter-Benutzer Sende Mail Zeige registrierte Benutzer Zeige Benutzer Betreff des E-Mails Die Route/REST-API ist Das ist nur ein Beispiel, wie man diese Liste im JSON-Format via AJAX bekommt. Wenn dieser Button geklickt wird, sieht man in der Konsole bzw. im Netzwerkfenster des Browsers beim Debuggen die angefragten Daten. Das Textfeld zeigt das JSON-Objekt als String an. Im Code kann man nachsehen, wie das genau funktioniert, falls man die REST-API verwenden möchte. Titel Wahr, wenn zum Newsletter angemeldet. Ja Benutzer wurde zum Newsletter registriert. Benutzer wurde vom Newsletter entfernt. Benutzer wurden zum Newsletter registriert. Benutzer wurden vom Newsletter entfernt. wird verwendet. 