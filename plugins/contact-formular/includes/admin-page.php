<?php

namespace plugins\contact_formular;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

const DOMAIN = 'contactformular';
const GROUP  = 'contact-formular';

class CfAdminPage {
	private $options;

	/**
	 * CfAdminPage constructor.
	 * Initialize admin menu
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'cf_add_menu' ) );
		add_action( 'admin_init', array( $this, 'cf_init' ) );
		add_action( 'plugins_loaded', array( $this, 'cf_plugins_loaded' ) );
	}

	/**
	 * No need for a whole menu.
	 * Instead use a option menu under "Options" of Wordpress
	 */
	public function cf_add_menu() {
		add_options_page( 'Contact Formular Options', __( 'Contact Formular Options', DOMAIN ), 'manage_options',
			'contact-formular',
			array( $this, 'cf_print_settings_menu' ) );

	}

	/**
	 * Initialize options menu
	 */
	function cf_init() {
		add_settings_section(
			'contact-formular-settings',
			__( 'Contact Formular Settings', DOMAIN ),
			function () { // anonymous function, because it's just one line
				_e( 'Here you can configure the Formular plugin.', DOMAIN ); // text of the option section
			},
			GROUP
		);

		add_settings_field(
			'cf-email',
			__( 'Email address to be used for the request', DOMAIN ),
			array( $this, 'cf_print_setting_field' ), // this is "outsourced" because it's not only one line
			GROUP,
			'contact-formular-settings'
		);

		register_setting( GROUP, 'cf-email' );

		// set default value
		add_option( 'cf-email', get_option( 'admin_email' ) );

	}

	/**
	 * Option field to print input
	 */
	function cf_print_setting_field() {
		$this->options = esc_attr( get_option( 'cf-email' ) );
		echo "<input id='cf_email' name='cf-email' size='40' type='email' value='{$this->options}' />";
	}

	/**
	 * Load localization file
	 */
	function cf_plugins_loaded() {
		load_plugin_textdomain( DOMAIN, false, '/contact-formular/includes/lang' );
	}

	/**
	 * Print option menu
	 */
	function cf_print_settings_menu() { ?>
        <div class="wrap">
            <form method="post" action="options.php">
				<?php
				settings_fields( GROUP );
				do_settings_sections( GROUP );
				submit_button();
				?>
            </form>
        </div> <!-- .wrap -->

		<?php
	}

}

