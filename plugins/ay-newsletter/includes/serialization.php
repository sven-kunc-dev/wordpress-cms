<?php

// http://localhost:8080/wordpress/wp-json/newsletter/users
// http://localhost:8080/wordpress/?rest_route=/newsletter/users
namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

/**
 * Content of route call
 * This function generates the JSON and give it back
 */
function nl_get_newsletter_user() {

	// ARGS is defined in "initialization.php"
	$users = get_users( ARGS );

	$array = array();
	foreach ( $users as $u ) {
		$array[ $u->ID ]['name']  = $u->user_nicename;
		$array[ $u->ID ]['role']  = $u->roles[0];
		$array[ $u->ID ]['email'] = $u->user_email;
	}

	return rest_ensure_response( $array );
}


/**
 * This function will be called from register_rest_route(). It handles the permission to call this route (REST API)
 */
function nl_check_permission() {
	return current_user_can( 'administrator' );
}

/**
 * Add own route to the REST API
 */
function nl_register_route() {
	register_rest_route( 'newsletter', '/users', array(
		'methods'             => \WP_REST_Server::READABLE,
		'callback'            => __NAMESPACE__ . '\\nl_get_newsletter_user',
		'permission_callback' => __NAMESPACE__ . '\\nl_check_permission'
	) );
}

add_action( 'rest_api_init', __NAMESPACE__ . '\\nl_register_route' );
