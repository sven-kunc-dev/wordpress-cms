<?php

namespace finProfs;

function initPostType() {

	$labels = array(
		"name" => __( "Professors", "fin-profs" ),
		"singular_name" => __( "Professor", "fin-profs" ),
		"menu_name" => __( "Professors", "fin-profs" ),
		"all_items" => __( "All professors", "fin-profs" ),
		"add_new" => __( "New professor", "fin-profs" ),
		"add_new_item" => __( "Add new professor", "fin-profs" ),
		"edit_item" => __( "Edit professor", "fin-profs" ),
		"new_item" => __( "New professor", "fin-profs" ),
		"view_item" => __( "View professor", "fin-profs" ),
		"view_items" => __( "View professors", "fin-profs" ),
		"search_items" => __( "Search porfessors", "fin-profs" ),
		"not_found" => __( "No professor found", "fin-profs" ),
	);

	$args = array(
		"label" => __( "Professors", "fin-profs" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "professor", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor" ),
	);

	register_post_type( "professor", $args );
	
	 if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array(
            'key' => 'group_5d078d58ca5b0',
            'title' => __('Proferssor contact information',"fin-profs"),
            'fields' => array(
                array(
                    'key' => 'field_5d078d6b0b100',
                    'label' => __('photo',"fin-profs"),
                    'name' => 'photo',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'field_5d078d9b0b101',
                    'label' => __('Subject',"fin-profs"),
                    'name' => 'subject',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5d078da30b102',
                    'label' => __("Email address", "fin-profs"),
                    'name' => 'email',
                    'type' => 'email',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'professor',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));

    endif;
}

 

?>
