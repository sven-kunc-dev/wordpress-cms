<?php
/**
 * This file just contains dynamic css elements and registers css
 */

/**
 * Register CSS files
 */
function enqueue_stylesheets() {
	wp_enqueue_style( 'style', get_template_directory_uri() . "/stylesheets/style.css" );
	wp_enqueue_style( "style-desktop", get_template_directory_uri() . "/stylesheets/style-desktop.css", array(),
		false, "only screen and (min-width: 1024px)" );
}


function th_theme_customize() { ?>
    <style type="text/css">
        .sidebar {
            background-color: <?php echo get_theme_mod('sidebar_background', '#FFFFFF'); ?>;
        }

        main {
	        background-color: <?php echo get_theme_mod('main_background', '#FFFFFF'); ?>;
        }

        #menu {
	        background-color: <?php echo get_theme_mod('menu_background', '#0046A0'); ?>;
        }

        .container > footer {
		    background-color: <?php echo get_theme_mod('footer_background', '#0046A0'); ?>;
	    }
    </style>
<?php }


function theme_customize_register( $wp_customize ) {
	$wp_customize->add_setting( 'sidebar_background', array(
		'default' => '#FFFFFF'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_background', array(
		'label'   => __( 'Sidebar - Background color', DOMAIN ),
		'section' => 'colors',
	) ) );


	$wp_customize->add_setting( 'main_background', array(
		'default' => '#FFFFFF'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_background', array(
		'label'   => __( 'Main Window - Background color', DOMAIN ),
		'section' => 'colors'
	) ) );

	$wp_customize->add_setting( 'menu_background', array(
		'default' => '#0046A0'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_background', array(
		'label'   => __( 'Menu panel - Color', DOMAIN ),
		'section' => 'colors'
	) ) );

	$wp_customize->add_setting( 'footer_background', array(
		'default' => '#0046A0'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_background', array(
		'label'   => __( 'Footer - Background color', DOMAIN ),
		'section' => 'colors'
	) ) );
}

