﻿<?php 
//include "functions.php"; wird von wp aufgerufen
?>
<!DOCTYPE html>

<!-- HEADER (body öffnen) -->
<?php get_header(); ?>

<!-- ----------------------------------------------------------------------------------------- -->

<!-- ------------------------ ANFANG CONTENT und Hauptbereich der Startseite des Prototypen-Theme von GrCMS im SS19 ------------------------ -->

<main> 

<!-- 3rd Menu: sidebar -->
<?php get_sidebar(); ?>


<!-- Menu: widget navigation VERSUCH
<div class="widgetnav">
<?php if ( is_active_sidebar( 'widget-side-bar' ) ) : ?>
    <?php dynamic_sidebar( 'widget-side-bar'); ?>
	<?php //wp_list_categories('orderby=name&order=ASC'); ?>
<?php endif; ?>
</div>
-->


<!-- Schleife über alle Posts, Beiträge, Content, Artikel - Darstellung aller Artikel als Grid möglich, siehe css -->
<div class="gridWrapper">	

<?php while(have_posts()): the_post(); ?>

<article class="gridBox" id="articleContent">
	<h2><?php the_title(); ?></h2>
	<div class="entry">
	
	<?php the_content(); ?>
	
	<div id="p_meta">
	
    Geschrieben von
	<!-- <a id="a_meta" href="<?php echo get_author_posts_url( 
										get_the_author_meta( 'ID' ),
										get_the_author_meta( 'user_nicename' ) ); ?>"> -->
	<?php the_author(); ?> 	
								
	am <?php echo get_the_date(); ?>
	</div>
	</div>
</article>

<?php endwhile; ?>

</div>

</main>


<!-- ------------------------------------------------------------------------------------------->
<!-- ------------------------ ENDE CONTENT und Hauptbereich der Startseite -------------------------->


<!-- SIDEBAR -->
<?php get_sidebar(); ?>


<!-- FOOTER -->
<?php get_footer(); ?>
