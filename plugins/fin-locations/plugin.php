<?php
/**
 * Plugin Name: Fakultät IN Locations
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: Use a clickable map as navigation
 * Author: jakob braun
 * Version: 0.1
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
