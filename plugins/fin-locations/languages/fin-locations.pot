# Copyright (C) 2019 jakob braun
# This file is distributed under the same license as the Fakultät IN Locations plugin.
msgid ""
msgstr ""
"Project-Id-Version: Fakultät IN Locations 0.1\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/fin-locations\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-05-28T11:53:25+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.2.0\n"
"X-Domain: fin-locations\n"

#. Plugin Name of the plugin
msgid "Fakultät IN Locations"
msgstr ""

#. Plugin URI of the plugin
msgid "https://github.com/ahmadawais/create-guten-block/"
msgstr ""

#. Description of the plugin
msgid "Use a clickable map as navigation"
msgstr ""

#. Author of the plugin
msgid "jakob braun"
msgstr ""

#: src/block/ImageMapEditor.js:49
msgid "The text must contain exactly one <map> tag!"
msgstr ""

#: src/block/ImageMapEditor.js:55
msgid "Please specify at least one area!"
msgstr ""

#: src/block/ImageMapEditor.js:62
msgid "Please specify a title for each area!"
msgstr ""

#: src/block/ImageMapEditor.js:92
msgid "Set Image Map:"
msgstr ""

#: src/block/ImageMapEditor.js:93
#: src/block/ImageMapEditor.js:97
msgid "Insert image map"
msgstr ""

#: src/block/ImageMapEditor.js:99
msgid "Insert the HTML code of the image map here. Please set the title attributes. We recomend to use an external tool like: "
msgstr ""

#: src/block/ImageMapEditor.js:103
msgid "OK"
msgstr ""

#: src/block/ImageMapEditor.js:104
msgid "Cancel"
msgstr ""

#: src/block/block.js:46
msgid "FIN Location Map"
msgstr ""

#: src/block/block.js:50
msgid "Map"
msgstr ""

#: src/block/block.js:51
msgid "Navigation"
msgstr ""

#: src/block/block.js:123
msgid "Select a background image:"
msgstr ""

#: src/block/block.js:130
msgid "Upload Image!"
msgstr ""

#: src/blocks.js:14
msgid "asdf"
msgstr ""
