<?php if ( is_page() || is_single() ): ?>
    <h2 class="article-title"> <?php the_title(); ?></h2> <!-- .article-title -->
<?php else: ?>
    <h2 class="article-title"><a href="<?php the_permalink() ?>"> <?php the_title(); ?></a></h2> <!-- .article-title -->
<?php endif; ?>