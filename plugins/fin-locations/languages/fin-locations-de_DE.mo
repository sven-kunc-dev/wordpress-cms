��          �      ,      �     �     �     �     �  w   �     A  
   E     P  %   S  !   y     �     �  ,   �     �  !         "  �  '  	   �     �     �       �        �  
   �     �  /   �  %   �          0  ,   C     p  '        �                                         
                                 	    Cancel FIN Location Map Fakultät IN Locations Insert image map Insert the HTML code of the image map here. Please set the title attributes. We recomend to use an external tool like:  Map Navigation OK Please specify a title for each area! Please specify at least one area! Select a background image: Set Image Map: The text must contain exactly one <map> tag! Upload Image! Use a clickable map as navigation asdf Project-Id-Version: Fakultät IN Locations 0.1
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/fin-locations
POT-Creation-Date: 2019-05-28 13:53+0200
PO-Revision-Date: 2019-05-28 13:53+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Domain: fin-locations
Plural-Forms: nplurals=2; plural=(n != 1);
 Abbrechen Standorte der FIN Orte der FIN Image Map einfügen Füge hier den HTML Code der Image Map ein. Bitte setzte dafür für alle Areas das title Attribut. Für die Erstellung verwendest du am Besten: Karte Navigation OK Bitte setzte für jede Area das title Attribut! Bitte definiere wenigstens eine area! Wähle ein Hintergrundbild: Image Map setzten: Der text muss genau ein <map> Tag enthalten! Bild hochladen Bietet interaktive Karte als Navigation Test 