<nav id="menu-footer">
	<?php wp_nav_menu( array(
		'theme_location' => 'footer-menu',
		'container'      => '',
		'depth'          => 1, // disallow sub/child menu for footer
	) ); ?>
</nav> <!-- #menu-footer -->