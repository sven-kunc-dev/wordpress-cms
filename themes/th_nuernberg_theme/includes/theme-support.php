<?php
// to make it possible that the <title> tag is set automatically
add_theme_support( 'title-tag' );

$defaults    = array(
	'height'      => 47,
	'width'       => 347,
	'flex-height' => false,
	'flex-width'  => false,
	'header-text' => array( 'site-title', 'th_nuernberg_theme-site-description' ),
);

// to make it possible to change the logo, but with fixed maximum size!
add_theme_support( 'custom-logo', $defaults );

// to change the color of background
add_theme_support( 'custom-background', [ 'default-color' => '#FFFFFF' ] );