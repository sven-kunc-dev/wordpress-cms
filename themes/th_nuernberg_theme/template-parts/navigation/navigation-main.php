<nav id="menu">
	<div id="menu-bar-wrapper">
		<div id="menu-content">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'main-menu',
				'container'      => '',
				'depth'          => 4, // 4 sub/child menus are enough
			) );
			?>
		</div> <!-- #menu-content -->
	</div> <!-- #menu-bar-wrapper -->
</nav> <!-- #menu -->