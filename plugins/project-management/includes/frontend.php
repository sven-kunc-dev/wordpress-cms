<?php

namespace plugins\project_management;

class pmFrontEnd {

	public function __construct() {
		add_shortcode( 'project_management', array( $this, 'pm_shortcode' ) );
	}

	private function pm_html_code() { ?>
        <form action="<?= esc_url( $_SERVER['REQUEST_URI'] ); ?>" method="POST">
            <select id="prof-select" name="prof-select">
                <option value="prof" selected>Prof</option>
                <option value="profa">ProfA</option>
                <option value="profb">ProfB</option>
                <option value="profc">ProfC</option>
            </select>
			<input type="submit" name="pm-submitted" value="Abschicken"/>
        </form>

		<?php
	}
	
	private function pm_send_form() {
		if ( isset( $_POST['pm-submitted'] ) ): ?>
			<p>Erfolgreich!</p>
			<?php
			/* sanitizing area
			....
			....
			....
			*/
		endif; 
	} 
	
	private function pm_javascript() { 
		wp_enqueue_script('formscript', plugin_dir_url(__FILE__) . '/scripts/form.js');
	?>

		
		
		
		<?php
	} 

	public function pm_shortcode() {
		// What to do if [project_management] is entered
		$this->pm_html_code();
		$this->pm_send_form();
		$this->pm_javascript();
	}
}