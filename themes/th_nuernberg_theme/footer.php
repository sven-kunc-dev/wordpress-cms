<?php
/**
 * This file is the main footer
 * It's included in all files
 */
?>
</main><!-- #content -->
<?php get_sidebar('right'); ?>
<footer>
	<?php get_template_part( 'template-parts/footer/footer', 'address' ); ?>
	<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
		<?php get_template_part( 'template-parts/navigation/navigation', 'footer' ); ?>
	<?php endif; ?>
</footer>

</div><!-- .container -->

<button onclick="go_to_top()" id="go-to-top" title="Gehe nach oben">⇑</button>

<?php wp_footer(); ?>

</body>
</html>