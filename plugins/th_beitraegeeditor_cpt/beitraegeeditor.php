<?php
/*
Plugin Name: Beiträge Editor der TH Nürnberg
Plugin URI: localhost/wordpress/
Version: 1.0
Author: Sven Kunc
Description: Beiträge custom post type editieren
*/

/*
* Ein Plugin welches ein Custom Post Type "OHM Events" hinzufügt + Autorenunterstüzung durch Shortcodes: Signatur, Impressum
*
*
*/


const DOMAIN = 'beitraegeeditor';

// Register Custom Post Type
function cpt_ohm_events() {
	$args = array(
		'label' => __( 'OHM Event', 'cpt_ohm_events' ),
		'description' => __( 'OHM Event listing', 'cpt_ohm_events' ),
		'labels' => $labels,
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields' ),
		'taxonomies' => array( 'category' ),
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 10,
		'menu_icon' => 'dashicons-welcome-learn-more',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page',
		'show_in_rest' => true,
	);
	
	$labels = array(
		'name' => _x( 'OHM Nuremberg Events', 'Post Type General Name', 'cpt_ohm_events' ),
		'singular_name' => _x( 'OHM Nuremberg Event', 'Post Type Singular Name', 'cpt_ohm_events' ),
		'menu_name' => __( 'OHM Nuremberg Events', 'cpt_ohm_events' ),
		'name_admin_bar' => __( 'OHM Nuremberg Events', 'cpt_ohm_events' ),
		'archives' => __( 'OHM Event Archives', 'cpt_ohm_events' ),
		'attributes' => __( 'OHM Event Attributes', 'cpt_ohm_events' ),
		'parent_item_colon' => __( 'Parent OHM Event:', 'cpt_ohm_events' ),
		'all_items' => __( 'All OHM Events', 'cpt_ohm_events' ),
		'add_new_item' => __( 'Add New OHM Event', 'cpt_ohm_events' ),
		'add_new' => __( 'Add New', 'cpt_ohm_events' ),
		'new_item' => __( 'New OHM Event', 'cpt_ohm_events' ),
		'edit_item' => __( 'Edit OHM Event', 'cpt_ohm_events' ),
		'update_item' => __( 'Update OHM Event', 'cpt_ohm_events' ),
		'view_item' => __( 'View OHM Event', 'cpt_ohm_events' ),
		'view_items' => __( 'View OHM Events', 'cpt_ohm_events' ),
		'search_items' => __( 'Search OHM Event', 'cpt_ohm_events' ),
		'not_found' => __( 'Not found', 'cpt_ohm_events' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'cpt_ohm_events' ),
		'featured_image' => __( 'OHM Event Image', 'cpt_ohm_events' ),
		'set_featured_image' => __( 'Set OHM Event image', 'cpt_ohm_events' ),
		'remove_featured_image' => __( 'Remove OHM Event image', 'cpt_ohm_events' ),
		'use_featured_image' => __( 'Use as OHM Event image', 'cpt_ohm_events' ),
		'insert_into_item' => __( 'Insert into OHM Event', 'cpt_ohm_events' ),
		'uploaded_to_this_item' => __( 'Uploaded to this OHM Event', 'cpt_ohm_events' ),
		'items_list' => __( 'OHM Events list', 'cpt_ohm_events' ),
		'items_list_navigation' => __( 'OHM Events list navigation', 'cpt_ohm_events' ),
		'filter_items_list' => __( 'Filter OHM Events list', 'cpt_ohm_events' ),
	);	
	register_post_type( 'OHM Events', $args );
	
}//end function for arraying all CPT meta, register it at the end
//Hinzufügen der Domain zur WP-Init-Funktion
add_action( 'init', 'cpt_ohm_events');
//end cpt bascis area


//TH Nuernberg Signatur Autorenunterstüzung 
function th_signature_print() {
	$ausgabe = '<div>			
<img>'.get_avatar( $current_user->ID, 20	).'</img>
<strong>Anschrift/Signatur</strong><br>
Technische Hochschule Nürnberg<br>
Keßlerplatz 12<br>
90489 Nürnberg<br>
Tel. 0911-5880-0<br>
Fax 0911-5880-8309<br>
E-Mail: infoatth-nuernbergPunktde<br>
Internet: www.th-nuernberg.de
</div>';
	return $ausgabe;
}
//Hinzufügen der Signatur-HTML-Struktur als WP-Shortcode
add_shortcode( 'th_nue_signature', 'th_signature_print' );

//TH Nuernberg Impressum Autorenunterstüzung 
function th_impressum_print() {
	$ausgabe =	/*_e(*/'<div>
	  <h2>Impressum</h2>
  
<!-- Wir --> 
  <article>
	
	<h3>Inhaber/Studenten</h3>
    <p>
	Braun, Jakob <br> 
	Yavas, Ayberk <br> 
	Kunc, Sven <br>
    </p>
	
  </article>
    
<!-- Beispielimpressum -->  
   <article> 
  <h3>Postanschrift  </h3>
    <p>
	Technische Hochschule Nürnberg Georg Simon Ohm<br>
    <b>Fakultät Informatik</b><br>
	Keßlerplatz 12<br>
    90489 Nürnberg<br>
	Tel. 0911-0000-01 <br>
	Fax. 0911-0000-02 <br>
	E-Mail: info@example.de <br>
	Internet: www.example.de <br>
	</p>
	<p>
	Handelsregister: Amtsgericht Scheinstadt Abt. XYZ HRA 1234 <br>
	Umsatzsteuer-ID: DE 123456789 Zuständige Kammer: IHK Nürnberg/Mittelfranken <br>
	</p> 
	--> 
   </article>

<!-- Haftungsausschlüsse -->    
  <article>
    <h3>Haftung für Inhalte</h3>
    <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p>
    <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
  </article>
  <article>
    <h3>Haftung für Links</h3>
    <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p>
    <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
  </article>
  <article>
    <h3>Urheberrecht</h3>
    <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</p>
    <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
  </article>
  <address>
    <p><i>Quelle: <a href="https://www.e-recht24.de" target="blank">eRecht24</a></i></p>
  </address>
  </div>';//, 'cpt_ohm_events') ;
	
	return $ausgabe;
}//end impressum
//Hinzufügen der Impressum-HTML-Struktur als WP-Shortcode
add_shortcode( 'th_nue_impressum', 'th_impressum_print' );



function myplugin_load_textdomain() {
  load_plugin_textdomain( 'cpt_ohm_events', false, basename( dirname( __FILE__ ) ) . '/languages' ); 
}

//add_action( 'plugins_loaded', 'myplugin_load_textdomain' );

//echo get_locale();


// }//End Class beitraegeeditor
// beitrageeditor::get_instance(); // Create instance

?>