<?php
get_header();

while (have_posts()): the_post(); ?>
    <?php get_template_part( 'template-parts/content/content', 'title' ); ?>
    <div id="profInfoBox">
        <?php echo "<img src='" . get_field("photo",$prof->ID) . "' />";?>
        <?php echo "<b>" . __("Subject", "fin-profs") . ":</b><br> " . get_field("subject", $prof->ID); ?> <br /><br>
        <?php echo "<b>" . __("Email address", "fin-profs") . ":</b><br> " . get_field("email", $prof->ID); ?> 
    </div>
    <article class="entry">
        <?php the_content(); ?>
    </article>
<?php endwhile; ?>
<?php
get_footer();
?>
         
