<?php
/*
Plugin Name: CMS Testplugin
Plugin URI: https://testseiteay.de
Version: 0.1
Author: Ay
Description: Does nothing
*/

namespace testplugin;

require_once('config.php');

const DOMAIN = 'testplugin';

class testplugin
{
    protected static $_instance = null; // Singleton instance

    public static function get_instance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    protected function __clone()
    {
    } // Prevent singleton cloning

    protected function __construct()
    {
        add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
        add_action('admin_init', array(&$this, 'admin_init'));
        add_action('admin_menu', array(&$this, 'admin_menu'));
    }

    function plugins_loaded()
    {
        if (false === get_option(SETTING_NAME_AD)) {
            add_option(SETTING_NAME_AD, SETTING_DEFAULT_AD);
        }

        load_plugin_textdomain(DOMAIN, false, dirname(plugin_basename(__FILE__)) . '/lang/');
    }

    function admin_menu()
    {
        add_options_page('CMS Testplugin', 'CMS Testplugin', 'manage_options', basename(__FILE__),
            array(&$this, 'print_settings_page'));
    }

    function admin_init()
    {
        add_settings_section(
            'cms_tutorial_settings_frontend',
            __('Frontend settings'),
            array(
                &$this,
                'print_settings_frontend'
            ),
            'cms_tutorial_settings'
        );
        add_settings_field(
            SETTING_NAME_AD,
            __('Announcement text'),
            array(&$this, 'print_setting_ad'),
            'cms_tutorial_settings',
            'cms_tutorial_settings_frontend'
        );
        register_setting('cms_tutorial_settings',
            SETTING_NAME_AD);
    }

    function print_settings_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die('You do not have sufficient permissions to manage options for this site.');
        } ?>
        <div class="wrap">
        <h2><?php echo(esc_html('Settings &rsaquo; CMS
tutorial')); ?></h2>
        <form method="POST" action="options.php"><?php
            settings_fields(
                'cms_tutorial_settings');
            do_settings_sections(
                'cms_tutorial_settings');
            submit_button();
            ?></form>
        </div><?php
    }

    function print_settings_frontend()
    {
        echo '<p>' . __('Settings affecting the frontend.', DOMAIN) . '</p>';
    }

    function print_setting_ad()
    {
        echo '<textarea
name="' . SETTING_NAME_AD . '"
id="' . SETTING_NAME_AD . '">'
            . esc_textarea(get_option(SETTING_NAME_AD))
            . '</textarea> ';
        ?><p class="description"><?php __('The text displayed in the announcement.') ?></p><?php
    }
}

testplugin::get_instance();

class CmsTutorialPlugin
{
    function print_ad()
    {
        echo('<span style="background-color: red;">' .
            get_option(SETTING_NAME_AD) . '</span>');
    }
}


?>