<?php

namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

const DOMAIN = 'newsletter';

const ARGS   = array(
	'meta_key'   => 'nl_registered',
	'meta_value' => true,
);

class NlInitialization extends \WP_Widget {

	private $user_id;

	/**
	 * NlInitialization constructor.
	 * Registers Widget to wordpress and adds menu
	 */
	public function __construct() {
		parent::__construct(
			'newsletter',
			__( 'Newsletter Widget', DOMAIN ),
			array( 'customize_selectice_refresh' => true ) );


	}

	/**
	 * Overrided widget function. This function renders the front end of the widget for user
	 */
	public function widget( $args, $instance ) {
		// to access to parameters like before_widget or before_title
		extract( $args );
		$this->user_id = get_current_user_id();

		$title = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';

		echo $before_widget;

		// Show title of widget with the custom element of register_sidebar
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		$this->nl_submit();
		?>
        <form action="<?= esc_url( $_SERVER['REQUEST_URI'] ); ?>" method="post">
			<?php if ( $this->user_id === 0 ): ?>
                <p>Sie m&uuml;ssen angemeldet sein, um sich für den Newsletter zu registrieren.</p>
			<?php else: ?>
				<?php if ( get_user_meta( $this->user_id, 'nl_registered', true ) ): ?>
                    <p><input type="hidden" name="nl-user" id="nl-user" value="<?= $this->user_id; ?>"/></p>
                    <p><input type="submit" name="nl-logout" value="Von Newsletter abmelden"/></p>
				<?php else: ?>
                    <p><input type="hidden" name="nl-user" id="nl-user" value="<?= $this->user_id; ?>"/></p>
                    <p><input type="submit" name="nl-register" value="F&uuml;r Newsletter anmelden"/></p>
				<?php endif; ?>
			<?php endif; ?>
        </form>
		<?= $after_widget; ?>
	<?php }

	/**
	 * Override form function. This is for the widget administration page
	 */
	public function form( $instance ) {
		$default = array( 'title' => __( 'Newsletter' ) );
		extract( wp_parse_args( ( array ) $instance, $default ) ); ?>
        <p>
            <label for="<?= esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', DOMAIN ); ?></label>
            <input class="widefat" id="<?= esc_attr( $this->get_field_id( 'title' ) ); ?>"
                   name="<?= esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?= $title; ?>"/>
        </p>
	<?php }

	/**
	 * Save users change for news letter
	 */
	function nl_submit() {
		if ( isset( $_POST['nl-register'] ) ):
			// update_user_meta returns true, if successful
			if ( update_user_meta( $_POST['nl-user'], 'nl_registered', true ) ): ?>
                <p>Erfolgreich angemeldet.</p>
			<?php else: ?>
                <p>Es ist ein Fehler aufgetreten.</p>
			<?php endif; ?>
		<?php elseif ( isset( $_POST['nl-logout'] ) ):
			// update_user_meta returns true, if successful
			if ( update_user_meta( $_POST['nl-user'], 'nl_registered', false ) ): ?>
                <p>Erfolgreich abgemeldet.</p>
			<?php else: ?>
                <p>Es ist ein Fehler aufgetreten.</p>
			<?php endif; ?>
		<?php endif;
	}

}

