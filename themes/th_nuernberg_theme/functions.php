<?php

const DOMAIN = 'th_theme';

include_once('includes/css.php'); // to load dynamic css element that user can change in admin panel
include_once('includes/theme-support.php'); // all theme support commands

/**
 * Register menus
 */
function register_menus() {
	register_nav_menus( array(
			'main-menu'   => __( 'Menu panel' ),
			'footer-menu' => __( 'Menu footer' )
		)
	);
}

/**
 * Register widgets
 */
function widget_own_init() {
	register_sidebar( array(
		'name'          => 'Rechte Sidebar',
		'id'            => 'right_sidebar',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2><div class="widget-content">', // no closing for div, because wordpress automatically closes the content of widget
	) );

}

function load_scripts() {
	wp_enqueue_script( 'top', get_template_directory_uri() . '/includes/scripts/top.js', array ( 'jquery' ), 1.1, false);
}


/**
 * Add localization file
 */
function load_translation() {
	load_theme_textdomain( DOMAIN, get_template_directory() . '/lang' );
}


/**
 * Change maximum result for search
 *
 * @param $q : query
 *
 * @return $q : Changed query
 */
function maximum_result_for_search( $q ) {
	if ( isset( $_REQUEST['s'] ) ) {
		$q['posts_per_page'] = 5;
	}

	return $q;
}

/**
 * Hooks
 */
add_filter( 'request', 'maximum_result_for_search' );
add_action( 'init', 'register_menus' );
add_action( 'wp_enqueue_scripts', 'enqueue_stylesheets' );
add_action( 'widgets_init', 'widget_own_init' );
add_action( 'customize_register', 'theme_customize_register' );
add_action( 'wp_head', 'th_theme_customize' );
add_action( 'after_setup_theme', 'load_translation' );
add_action( 'wp_enqueue_scripts', 'load_scripts' );