<?php
get_header();

if ( have_posts() ) {
	get_template_part( 'template-parts/content/content' );
} else {
	get_template_part( 'template-parts/content/content', 'none' );
}

get_footer();
?>

