<article <?php post_class() ?>>
	<?php the_content();
    if(!is_page()) {
	    get_template_part( 'template-parts/content/content', 'author-info' );
    }
	?>
</article>
