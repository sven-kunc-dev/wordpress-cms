<?php get_header(); ?>
<h1 class="article-title"> Alle Artikel von <?php the_author(); ?>
    <span class="user-avatar" style="margin-left: 20px; position: absolute;"><?php echo get_avatar( $author,
			42 ); ?></span>

</h1> <!-- .article-title -->
<?php get_template_part( 'template-parts/content/content' ); ?>
<?php get_footer(); ?>
