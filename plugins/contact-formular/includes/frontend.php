<?php

namespace plugins\contact_formular;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

class CfFrontEnd {

	/**
	 * CfFrontEnd constructor.
     * This adds the possibility to use shortcodes.
	 */
	public function __construct() {
		add_shortcode( 'contact_form', array( $this, 'cf_shortcode' ) );
	}

	/**
	 * HTML code of front end
	 */
	function cf_html_code() { ?>
        <form action="<?= esc_url( $_SERVER['REQUEST_URI'] ); ?>" method="post">
            <p>
                <label for="cf-name">
                    Name*<br/>
                    <input type="text" name="cf-name" id="cf-name" pattern="[a-zA-Z0-9 ]+"
                           value="<?php ( isset( $_POST["cf-name"] ) ? esc_attr( $_POST["cf-name"] ) : '' ); ?>"
                           size="40"/>
                </label>
            </p>
            <p>
                <label for="cf-email">
                    E-Mail-Adresse* <br/>
                    <input type="email" name="cf-email" id="cf-email"
                           value="<?php ( isset( $_POST["cf-email"] ) ? esc_attr( $_POST["cf-email"] ) : '' ) ?>"
                           size="40"/>
                </label>
            </p>
            <p>
                <label for="cf-subject">
                    Betreff* <br/>
                    <input type="text" name="cf-subject" id="cf-subject" pattern="[a-zA-Z ]+"
                           value="<?php ( isset( $_POST["cf-subject"] ) ? esc_attr( $_POST["cf-subject"] ) : '' ) ?>"
                           size="40"/>
                </label>
            </p>
            <p>
                <label for="cf-message">
                    Nachricht* <br/>
                    <textarea rows="10" cols="35" name="cf-message"
                              id="cf-message"><?php ( isset( $_POST["cf-message"] ) ? esc_attr( $_POST["cf-message"] ) : '' ) ?> </textarea>
                </label>
            </p>
            <p><input type="submit" name="cf-submitted" value="Senden"/></p>
        </form>
		<?php
	}

	/**
	 * Sending submitted mail to admin
	 */
	function cf_send_mail() {
		if ( isset( $_POST['cf-submitted'] ) ) {
			if(!empty($_POST['cf-name']) && !empty($_POST['cf-email']) && !empty($_POST['cf-subject']) && !empty($_POST['cf-message'])) {
				// sanitize form values
				$name    = sanitize_text_field( $_POST['cf-name'] );
				$email   = sanitize_email( $_POST['cf-email'] );
				$subject = sanitize_text_field( $_POST['cf-subject'] );
				$message = esc_textarea( $_POST['cf-message'] );

				$to = get_option( 'cf-email' );

				$headers = "From: $name <$email>" . "\r\n"; ?>

				<?php if ( wp_mail( $to, $subject, $message, $headers ) ): ?>
                    <div>
                        <p>Danke, dass du uns kontaktiert hast. Wir melden uns bei dir.</p>
                        <p>Nur f&uuml;r Debug-Zwecke: Konfigurierte E-Mail lautet <?= get_option( 'cf-email' ); ?></p>
                    </div>
				<?php else: ?>
                    Es ist ein Fehler aufgetreten.
                    <p>Nur f&uuml;r Debug-Zwecke: Konfigurierte E-Mail lautet <?= get_option( 'cf-email' ); ?></p>
				<?php endif;
            } else { ?>
			    <p>Alle Felder sind Pflichtfelder.</p>
           <?php }

		}
	}

	/**
	 * What should happen if Wordpress recognize defined shortcode?
	 */
	function cf_shortcode() {
		ob_start();

		$this->cf_send_mail();
		$this->cf_html_code();

		return ob_get_clean();
	}
}

?>