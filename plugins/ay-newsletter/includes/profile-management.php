<?php

namespace plugins\newsletter;

defined( 'ABSPATH' ) or die( 'No direct access to file' );

class NlProfileManagement {

	public function __construct() {
		// to show new user metadata (nl_registered) in profile of users
		add_action( 'show_user_profile', array( $this, 'nl_show_in_profile' ) );
		add_action( 'edit_user_profile', array( $this, 'nl_show_in_profile' ) );

		// to save changes in profile
		add_action( 'personal_options_update', array( $this, 'nl_save_in_profile' ) );
		add_action( 'edit_user_profile_update', array( $this, 'nl_save_in_profile' ) );
	}

	/**
	 * This function save changes in user profile
     * Check for permission (not really necessary here) and update user meta (true or false)
	 */
	function nl_save_in_profile( $user_id ) {
		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}
		if ( isset( $_POST['nl-user-profile'] ) ) {
			$check = true;
		} else {
			$check = false;
		}
		update_user_meta( $user_id, 'nl_registered', $check );

		return true;
	}

	/**
	 * This function shows new user meta field in profile
     * Basic HTML output
	 */
	function nl_show_in_profile( $user ) { ?>
        <table class="form-table">
            <tr>
                <th><label for="nl-user-profile"><?php _e( 'Registered to Newsletter', DOMAIN ); ?></label></th>
                <td>
					<?php $check = get_user_meta( $user->ID, 'nl_registered', true ); ?>

                    <input type="checkbox" name="nl-user-profile" id="nl-user-profile" value="nl-registered"
                           class="regular-text" <?= $check == true ? 'checked' : '' ?>/><br/>
                    <span class="description"><?php _e( 'True, if registered to newsletter.', DOMAIN ); ?></span>
                </td>
            </tr>
        </table>
		<?php
	}
}