<?php
/**
 * This file is the main header
 * It's included in all files
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php get_template_part( 'template-parts/header/header', 'meta' ); ?>
<body <?php body_class(); ?>>
<div class="container">
    <header id="header">
        <div class="wrapper">
			<?php
			get_template_part( 'template-parts/header/header', 'logo' );
			if ( ( get_theme_mod( 'header_text' ) !== 0 ) ): ?>
                <span class="blog-name"><?php
					bloginfo( 'name' ); ?> - </span>
                <span class="blog-description"> <?php bloginfo( 'description' ); ?></span>
			<?php endif; ?>
            <div class="search-wrapper">
                <h2 class="search-text">Suche</h2>
                <p>
                <form class="search-form" method="get" id="searchform" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <input type="text" value="<?php echo esc_html( $s ); ?>" name="s" id="s"/>
                    <input type="submit" id="search_submit" value="Suchen"/>
                </form> <!-- .search-form -->
                </p>
            </div> <!-- .search-wrapper -->
        </div> <!-- .wrapper -->
		<?php get_template_part( 'template-parts/navigation/navigation', 'main' ); ?>
    </header> <!-- #header -->
    <main id="content">