(function ($) {
    $('#ajax-tester').click(function () {
        // to access to the parameters
        let settings = Settings;
        $.ajax({
            // access to parameter path and call the route
            url: settings.path + '/wp-json/newsletter/users',
            // before sending the request it is necessary to set "X-WP-Nonce" parameter in header
            // If you don't do that, then the request will fail because you don't have permission for!
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', settings.nonce);
            }
            // do something with promise/response
        }).done(function (response) {
            $('#ajax-result').val(JSON.stringify(response));
            console.log(response);
        });

    });

})(jQuery);
