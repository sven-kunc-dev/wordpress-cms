<?php

defined( 'ABSPATH' ) or die( "Cannot access directly" );

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' ); // to use dbDelta

global $wpdb;
$charset_collate = $wpdb->get_charset_collate();

$table_name = $wpdb->prefix . "pm_projects";

$sql = "CREATE TABLE IF NOT EXISTS $table_name (
  p_pk_id int NOT NULL AUTO_INCREMENT,
  p_name tinytext NOT NULL,
  p_semester int NOT NULL,
  p_professor int NOT NULL,
  p_student int NOT NULL,
  PRIMARY KEY  (p_pk_id)
) $charset_collate;";


dbDelta( $sql );

$table_name = $wpdb->prefix . "pm_documents";
$sql        = "CREATE TABLE  IF NOT EXISTS $table_name (
	d_pk_id int NOT NULL AUTO_INCREMENT,
	d_fk_project_id int NOT NULL,
	PRIMARY KEY (d_pk_id),
	FOREIGN KEY (d_fk_project_id)
        REFERENCES " . $wpdb->prefix . "pm_projects(p_pk_id)
        ON DELETE CASCADE
	) $charset_collate;";

dbDelta( $sql );

$wpdb->query( $sql );
