<?php

/**
 * Plugin Name: Fakultät IN professors
 * Description: Create a page for each prof
 * Author: jakob braun
 * Version: 0.1
 *
 * @package fin
 */
 
namespace finProfs;

require("php/postType.php");

wp_enqueue_style( "profs_sytle", plugins_url( '/css/fin-profs.css', __FILE__ ));

//inject the overfiew page for professoren.
add_filter( 'page_template', function( $page_template )
{
    if ( is_page( 'professoren' ) ) {
        $page_template = dirname( __FILE__ ) . '/php/all.php';
    }
    return $page_template;
});

//inject the single page template for post type professor
add_filter( 'single_template', function( $page_template ){
    if(get_post_type() == "professor"){
        return dirname( __FILE__ ) . '/php/single.php';
    }
    return $page_template;
});

//add localization
function fin_load_plugin_textdomain() {
    load_plugin_textdomain( 'fin-profs', false, basename( dirname( __FILE__ ) ) . '/languages' ); //must be done before the post type is loaded for initernationalization to work
    initPostType();//see php/postType.php
}
add_action( 'init', 'finProfs\fin_load_plugin_textdomain' );

?>
