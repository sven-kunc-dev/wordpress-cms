<?php
//////////////////////////////////////////////////////////////
//HILFREICHES ZUERST	
// Cookie Hook zur 'init' Aktion
add_action( 'init', 'add_Cookie' ); 
// cookie setzen mit aktueller Tageszeit, Aufruf über den Hook add_action() nachdem Wordpress den Code fertiggeladen hat
function add_Cookie() {
  setcookie("last_visit_time", date("r"), time()+60*60*24*30, "/");
}

//Wordpress Codex Theme Support for content-width
if ( ! isset( $content_width ) ) {
	$content_width = 333;
}

// /////////////////////////////////////////////////////////
//FUNCTIONS.PHP PRIMARY STUFF

	//Registrieren der eigenen Menüs bei Wordpress: register_navs_menu
	register_nav_menu("primary", "Hauptnavigation");
	register_nav_menu("secondary", "Fußzeilennavigation");
	register_nav_menu("temporary", "Sidebarnavigation");
	// register_sidebar( "sidebar","Sidebarnavigation2");
	
	//WIDGET! EXAMPLE
	//register widgets for sidebar
	// function my_widget_sidebar() {
	 // register_sidebar( array (
// 'name' => __( 'Primary Widget Area'),
// 'id' => 'widget-side-bar',
// 'description' => __( "The primary widget area"),
// 'before_widget' => '<li id=”%1$s" class="widget-container %2$s”>',
// 'after_widget' => '</li>',
// 'before_title' => '<h3 class=”widget-title”>',
// 'after_title' => '</h3>',
// ) );
// }
// add_action( 'widgets_init', 'my_widget_sidebar' );
	
	
	//USER-ROLLEN: User, Studierende/r, Professor/in, Dekan
	 add_role( 'custom_user', 'Normaler User', array( 'read' => true, 'edit_posts' => false ) );
	 add_role( 'student', 'Studierende/r', array( 'read' => true, 'edit_posts' => false ) );
	 add_role( 'prof', 'Professor/in', array( 'read' => true, 'edit_posts' => true ) );
	 add_role( 'boss', 'Dekan/Chef', array( 'read' => true, 'edit_posts' => true ) );
	
	//PLUGIN-UPDATING: Setzen der Update-Hierarchien: Nur der Benutzer "root" darf Updates von Plugins durchführen
	//filter/hide plugin update notifications: only admin-login with user-ID "root" can make plugin updates
	//here:	akismet antispam and cube..., (for example to affect customer dependece)
	global $user_login;
    get_currentuserinfo();
 if ($user_login !== "root") { 				//set update rights by if clause
  function filter_plugin_updates( $value ) {
    unset( $value->response['akismet/akismet.php'] ); 				//plugin
	unset( $value->response['cube/cube.php'] );						//plugin
    return $value;
  }
  add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );
}



?>
