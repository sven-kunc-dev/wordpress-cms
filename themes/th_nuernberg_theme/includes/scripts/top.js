(function ($) {
    $(window).scroll(function () {
        let scroll = $(window).scrollTop();

        if (scroll > 100) {
            $('#go-to-top').show();
        } else {
            $('#go-to-top').hide();
        }
    });
})(jQuery);

function go_to_top() {
    window.scrollTo(0, 0);
}