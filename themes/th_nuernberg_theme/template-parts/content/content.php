<?php while ( have_posts() ): the_post(); ?>
	<?php get_template_part( 'template-parts/content/content', 'title' ); ?>
	<?php get_template_part( 'template-parts/content/content', 'entry' ); ?>
	<?php
	// check for last post. If true, then no need for an underline
	if ( ( $wp_query->current_post + 1 ) != ( $wp_query->post_count ) ): ?>
		<div class="underline"></div>
	<?php endif; ?>
<?php endwhile; ?>