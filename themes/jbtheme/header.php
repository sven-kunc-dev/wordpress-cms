<?php
?>
<!DOCTYPE html >
<html>
    <head>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type');   ?> ;charset=<?php bloginfo('charset');  ?>" />
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" type="text/css" media="screen" />
        <?php wp_head(); ?>
    </head> 
    <body>
    <div class="row">
        <div id="cont" class="col-md-6 offset-md-2">
            <?php wp_nav_menu(array( 'theme_location' => 'primary' )); ?> 
        </div>
    </div>
    <div id="main">
