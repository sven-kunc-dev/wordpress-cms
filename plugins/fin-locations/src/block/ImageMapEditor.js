
import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
const { __ } = wp.i18n;

class ImageMapEditor extends Component {
	constructor(props) {
		super(props);

		var map = null;
		if(this.props.imageMapHtml != null && this.props.imageMapHtml != ""){
			var parser = new DOMParser();
			var htmlDoc = parser.parseFromString(this.props.imageMapHtml,"text/html");
			var maps = htmlDoc.getElementsByTagName("map");
			if(maps.length == 1){
				map = maps[0];
			}
		}

		this.state = {
			modal: false,
			imgMapInput:"",
			imageMap: map,
			pages: []
		};

		//load all available pages and store them into pages
		var t = this;

		jQuery.get(wp_data.siteurl + "/index.php?rest_route=/wp/v2/pages/",{},function(res){
				t.setState({
					pages: res
				});
		});
	}

	//toggles the visibility of tjhe modal window
	toggleModal(){
		this.setState({
			modal: !this.state.modal
		})
	}

	setImgMapInput(e) {
		this.setState({
			imgMapInput:e.target.value
		});
	}

	//Tests and sets a new image map from html
	submitImageMap(){
		var parser = new DOMParser();
		var htmlDoc = parser.parseFromString(this.state.imgMapInput,"text/html");
		var maps = htmlDoc.getElementsByTagName("map");
		if(maps.length != 1){
			alert(__("The text must contain exactly one <map> tag!", "fin-locations"));
			return;
		}

		var areas = maps[0].getElementsByTagName("area");
		if(areas.length < 1){
			alert(__("Please specify at least one area!", "fin-locations"));
			return;
		}

		for(var areaIndx = 0; areaIndx < areas.length; areaIndx++){
			var title = areas[areaIndx].getAttribute("title");
			if(title == null || title == ""){
				alert(__("Please specify a title for each area!", "fin-locations"));
				return;
			}
		}

		maps[0].setAttribute("name","finImgMap");

		this.setState({
			imageMap: maps[0]
		},this.emitImageMap);

		this.toggleModal();
	}

	//propagates a changed imageMap
	emitImageMap(){
		this.props.imageMapHtmlChanged(this.state.imageMap.outerHTML);
	}

	pageSelected(evt,area){
		area.setAttribute("href",evt.target.value);
		this.emitImageMap();
	}

	renderPageSelect(area){
		var options = [];
		if(this.state.pages != null)
			options = this.state.pages.map(page => <option value={page.link}>{page.title.rendered}</option>);
		return (<select value={area.getAttribute('href')} onChange={function(evt){this.pageSelected(evt, area) }.bind(this) } >
			<option value="">---</option>
			{options}
		</select>);
	}


	renderPageSelects({pages}){
		if(this.state.imageMap == null)
			return "";
		var map = this.state.imageMap;
		var areas = map.getElementsByTagName("area");
		return Array.from(areas).map(area => {
				return (
					<p>
						{area.getAttribute("title")}:<br/>
						{this.renderPageSelect(area)}

					</p>
				);
		});
	}

	render() {
	return (
		<div>
			<div class="my-2">
				<strong>{__("Set Image Map:", "fin-locations")}</strong><br />
				<Button onClick={this.toggleModal.bind(this)}>{__('Insert image map', "fin-locations")}</Button>
			</div>

			<Modal isOpen={this.state.modal} toggleModal={this.toggleModal.bind(this)} >
			<ModalHeader toggleModal={this.toggleModal.bind(this)}>{__('Insert image map', "fin-locations")}</ModalHeader>
			<ModalBody>
				{__('Insert the HTML code of the image map here. Please set the title attributes. We recomend to use an external tool like: ', "fin-locations")} <a href="https://www.image-map.net/" target="_blank">image-map.net</a><br />
				<Input type="textarea" name="text" id="exampleText"  value={this.state.imgMapInput}  onChange={this.setImgMapInput.bind(this)} />
			</ModalBody>
			<ModalFooter>
				<Button color="primary" onClick={this.submitImageMap.bind(this)}>{__("OK","fin-locations")}</Button>{' '}
				<Button color="secondary" onClick={this.toggleModal.bind(this)}>{__("Cancel","fin-locations")}</Button>
			</ModalFooter>
			</Modal>

			{this.renderPageSelects({pages: []})}
		</div>
	);
	}
}

export default ImageMapEditor;
