<span class="logo">
                <?php
                if ( has_custom_logo() ): ?>
                    <a id="logo-link" href="<?php echo site_url(); ?>"><?php the_custom_logo(); ?></a>
                <?php else: ?>
                    <a id="logo-link" href="<?php echo site_url(); ?>"><img
                                src="<?= get_stylesheet_directory_uri() . '/images/logo.png' ?> "></a>
                <?php endif; ?>
</span> <!-- .logo -->